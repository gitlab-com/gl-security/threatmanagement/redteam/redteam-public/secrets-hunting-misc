#!/usr/bin/env python3

import subprocess

DRY_RUN_INDICATOR = "DryRun flag is set"

commands = [
    ["aws", "sts", "get-caller-identity"],
    ["aws", "iam", "list-users"],
    ["aws", "iam", "list-groups"],
    ["aws", "iam", "list-roles"],
    ["aws", "iam", "list-policies"],
    ["aws", "acm", "list-certificates"],
    ["aws", "ec2", "describe-instances"],
    ["aws", "ec2", "describe-vpcs"],
    ["aws", "ec2", "describe-volumes"],
    ["aws", "ec2", "describe-subnets"],
    ["aws", "ec2", "describe-security-groups"],
    ["aws", "ec2", "describe-key-pairs"],
    ["aws", "ec2", "create-key-pair", "--dry-run", "--key-name", "TestKP"],
    ["aws", "ec2", "run-instances", "--dry-run", "--image-id", "ami-0f7919c33c90f5b58", "--count", "1", "--instance-type", "t2.micro"],
    ["aws", "route53", "list-hosted-zones"],
    ["aws", "route53domains", "list-domains"],
    ["aws", "acm", "list-certificates"],
]

def check_access(cmd):
    cmd_text = " ".join(cmd)
    aws = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    output, errors = aws.communicate()
    aws.wait()
    if len(errors) > 0 and errors.count(DRY_RUN_INDICATOR) == 0:
        print(f"[!] Command '{cmd_text}' failed. ({errors})")
    else:
        print(f"[*] Command '{cmd_text}' succeeded.")



def main():
    try:
        aws_config = subprocess.Popen(["aws", "configure"])
        aws_config.wait()
        list(map(check_access, commands))
    except KeyboardInterrupt:
        print("[!] Execution cancelled from keyboard interrupt!")


if __name__ == '__main__':
    main()

